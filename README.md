# BioFVM-X (based on BioFVM v 1.1.6) 

[[_TOC_]]

## Introduction
This is a repository of code and analyses related to the paper "*BioFVM-X: An MPI+OpenMP 3-D Simulator for Biological Systems*" from BSC. The code is distributed under **BSD-3 Clause license**, as can be seen in the `./licenses/` folder.
<!-- The paper can be accessed here: link_to_come. -->

This repository can be cited with its own DOI: [![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.4723800.svg)](https://doi.org/10.5281/zenodo.4723800)

A step-by step walkthrough has been added for two different examples.

The motivation of this work is that BioFVM (http://BioFVM.MathCancer.org) supports only on-node shared memory parallelism using OpenMP. We have developed BioFVM_X as it adds MPI to BioFVM to parallelize the core kernels of BioFVM using MPI. Some core kernels are: Domain Partitioning, Basic Agent Generation, Gaussian Profile building, File Writing, Direct Solver (Thomas' algorithm). Most of the MPI-based parallel functions are made available in `BioFVM_parallel.cpp`.
  
## Minimal requirements to run the code

BioFVM-X has only two dependencies:

1. A C++ compiler (for e.g. GCC C++, Intel C++, Clang C++, IBM C++)
2. An MPI implementation (for e.g. OpenMPI, MPICH, MvaPICH2, Intel MPI, IBM MPI)

We use the GCC/8.1.0 C++ compiler and OpenMPI/3.1.1 MPI implementation for all our experiments. 

## Running the example tutorial1_BioFVM.cpp in ./examples/ 
  
A "general" Makefile is given in the root folder and a sample (parallelized) example in `./examples/tutorial1_BioFVM.cpp` is completely parallelized using MPI + OpenMP. 
  
The procedure to run:
1. git clone the repository
1. Change to the top level directory
1. Make sure GCC 8.1 (or any other version) and OpenMPI 3.1.1 (or any other version) are present (or the modules are loaded in a module environment if you have a module environment)
1. Execute `$ make tutorial1` from the root folder. This results in an executable file `tutorial1` in the directory `./examples/`.
      
Example output of the compilation:
      
```bash
bsc99102@login1:~/> make tutorial1
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_vector.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_matlab.cpp
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_utilities.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_mesh.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_microenvironment.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_solvers.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_basic_agent.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_agent_container.cpp 
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_MultiCellDS.cpp
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c BioFVM_parallel.cpp
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -c pugixml.cpp
mpic++ -march=native  -O2 -fomit-frame-pointer -mfpmath=both -fopenmp -m64 -std=c++11 -g  -o ./examples/tutorial1 ./examples/tutorial1_BioFVM.cpp BioFVM_vector.o BioFVM_matlab.o BioFVM_utilities.o BioFVM_mesh.o BioFVM_microenvironment.o BioFVM_solvers.o BioFVM_basic_agent.o BioFVM_agent_container.o BioFVM_MultiCellDS.o BioFVM_parallel.o  pugixml.o
```

Check if the executable has been made:

```bash
bsc99102@login1:~/> ls -lrt ./examples | grep tutorial1
-rw-r--r-- 1 bsc99102 bsc99     420 Sep  4  2019 tutorial1_BioFVM.h
-rw-r--r-- 1 bsc99102 bsc99   34502 Feb 14 13:46 tutorial1_BioFVM.cpp
-rwxr-xr-x 1 bsc99102 bsc99 6991728 Apr 16 16:03 tutorial1
```
5. For the SLURM scheduler, the `script_tutorial1.sh` script can be used:
 
```bash
#!/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=2
#SBATCH --cpus-per-task=24
#SBATCH -t 00:15:00
#SBATCH -o output-%j
#SBATCH -e error-%j
#SBATCH --exclusive

export OMP_DISPLAY_ENV=true
export OMP_NUM_THREADS=24
export OMP_PROC_BIND=spread
export OMP_PLACES=threads
mpiexec --map-by ppr:1:socket:pe=24  --report-bindings ./examples/tutorial1
```

This script :
- uses 1 single node (`--nodes=1`)
- spawns 2 MPI processes (`--ntasks-per-node=2`) in that node (Since we have 2 sockets per node, we create 2 MPI processes i.e. 1 MPI process per socket).
- create 24 threads (`--cpus-per-task=24`) per MPI process  (Since we have 24 cores in 1 socket, we create 24 threads i.e. 1 thread per core).

**IMPORTANT**: 
- **PLEASE MAKE SURE** that the dimension of the Physical domain (specified in `./examples/tutorial1_BioFVM.cpp`) is completely divisible by the Voxel dimensions (given by 'mesh_resolution' in `./examples/tutorial1_BioFVM.cpp`).
- **PLEASE MAKE SURE** that the number of Voxels in the X direction are PERFECTLY divisible by the total number of MPI processes. 

For the given example: `N=1000, minX=0, maxX=N, mesh_resolution=10`. This gives total Voxels in X direction as = `(maxX-minX)/mesh_resolution = (1000 - 0)/10 = 100` Voxels.

Now the total number of Voxels in the X-direction must be PERFECTLY divisible by the total number of MPI processes (in this case total MPI processes `P=2`). Thus, we have `100/2 = 50` Voxels per process in the x-direction. 

6. Submit the batch script to SLURM using: `$sbatch script_tutorial1.sh`
7. After the simulation completes, two text files are (generally) produced. One is the error file that contains the bindings of MPI processes and threads:

For example, the contents of the error file are:
```bash
[s05r2b10:340036] MCW rank 0 bound to socket 0[core 0[hwt 0]], socket 0[core 1[hwt 0]], socket 0[core 2[hwt 0]], socket 0[core 3[hwt 0]], socket 0[core 4[hwt 0]], socket 0[core 5[hwt 0]], socket 0[core 6[hwt 0]], socket 0[core 7[hwt 0]], socket 0[core 8[hwt 0]], socket 0[core 9[hwt 0]], socket 0[core 10[hwt 0]], socket 0[core 11[hwt 0]], socket 0[core 12[hwt 0]], socket 0[core 13[hwt 0]], socket 0[core 14[hwt 0]], socket 0[core 15[hwt 0]], socket 0[core 16[hwt 0]], socket 0[core 17[hwt 0]], socket 0[core 18[hwt 0]], socket 0[core 19[hwt 0]], socket 0[core 20[hwt 0]], socket 0[core 21[hwt 0]], socket 0[core 22[hwt 0]], socket 0[core 23[hwt 0]]: [B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B][./././././././././././././././././././././././.]

[s05r2b10:340036] MCW rank 1 bound to socket 1[core 24[hwt 0]], socket 1[core 25[hwt 0]], socket 1[core 26[hwt 0]], socket 1[core 27[hwt 0]], socket 1[core 28[hwt 0]], socket 1[core 29[hwt 0]], socket 1[core 30[hwt 0]], socket 1[core 31[hwt 0]], socket 1[core 32[hwt 0]], socket 1[core 33[hwt 0]], socket 1[core 34[hwt 0]], socket 1[core 35[hwt 0]], socket 1[core 36[hwt 0]], socket 1[core 37[hwt 0]], socket 1[core 38[hwt 0]], socket 1[core 39[hwt 0]], socket 1[core 40[hwt 0]], socket 1[core 41[hwt 0]], socket 1[core 42[hwt 0]], socket 1[core 43[hwt 0]], socket 1[core 44[hwt 0]], socket 1[core 45[hwt 0]], socket 1[core 46[hwt 0]], socket 1[core 47[hwt 0]]: [./././././././././././././././././././././././.][B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B/B]

OPENMP DISPLAY ENVIRONMENT BEGIN
  _OPENMP = '201511'
  OMP_DYNAMIC = 'FALSE'
  OMP_NESTED = 'FALSE'
  OMP_NUM_THREADS = '24'
  OMP_SCHEDULE = 'DYNAMIC'
  OMP_PROC_BIND = 'SPREAD'
  OMP_PLACES = '{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23}'
  OMP_STACKSIZE = '0'
  OMP_WAIT_POLICY = 'PASSIVE'
  OMP_THREAD_LIMIT = '4294967295'
  OMP_MAX_ACTIVE_LEVELS = '2147483647'
  OMP_CANCELLATION = 'FALSE'
  OMP_DEFAULT_DEVICE = '0'
  OMP_MAX_TASK_PRIORITY = '0'
OPENMP DISPLAY ENVIRONMENT END

OPENMP DISPLAY ENVIRONMENT BEGIN
  _OPENMP = '201511'
  OMP_DYNAMIC = 'FALSE'
  OMP_NESTED = 'FALSE'
  OMP_NUM_THREADS = '24'
  OMP_SCHEDULE = 'DYNAMIC'
  OMP_PROC_BIND = 'SPREAD'
  OMP_PLACES = '{24},{25},{26},{27},{28},{29},{30},{31},{32},{33},{34},{35},{36},{37},{38},{39},{40},{41},{42},{43},{44},{45},{46},{47}'
  OMP_STACKSIZE = '0'
  OMP_WAIT_POLICY = 'PASSIVE'
  OMP_THREAD_LIMIT = '4294967295'
  OMP_MAX_ACTIVE_LEVELS = '2147483647'
  OMP_CANCELLATION = 'FALSE'
  OMP_DEFAULT_DEVICE = '0'
  OMP_MAX_TASK_PRIORITY = '0'
OPENMP DISPLAY ENVIRONMENT END
```

and the second file is that of the output.

For example, the contents of the output file are:
```bash
TIME FOR RESIZING MICROENVIRONMENT = 1.0491
TIME FOR GENERATING GAUSSIAN PROFILE = 0.011721
TIME FOR WRITING INITIAL CONCENTRATION FILE = 0.154976
TIME FOR CREATING ALL BASIC AGENTS = 0.000926388
TIME FOR SIMULATING (SOURCES+SINKS+DIFFUSION) = 2.60654
TIME FOR WRITING FINAL FILE = 0.170324
TOTAL PROGRAM EXECUTION TIME = 3.99776
```

8. The simulation can/will produce .mat files (MATLAB files) as outputs. We produce at least 2 .mat files, one at the beginning and the second at the end of the simulation. The location where the files are produced can be controlled from the `./examples/tutorial1_BioFVM.cpp` file (see below the relevant lines in `./example/tutorial1_BioFVM.cpp`).
**PLEASE MAKE SURE** that if there are multiple calls to the function `write_to_matlab(...)`, then ALL the calls contain the path at which the user wants to store/produce the .mat files. 

For example:

```cpp
int main(int argc, char *argv[ ])
....
microenvironment.write_to_matlab( "./output_folder_name/initial_concentration.mat", mpi_Rank, mpi_Size, mpi_Cart_comm );
...
```

  9. This .mat file can be plotted using the MATLAB software (or Octave - as in our case). The .m help scripts for plotting can be found in the `./matlab/` folder.

Any of the scripts can be tried; we plotted using `cross_section_colormap.m` and `cross_section_surface.m`.

For example, inside the `cross_section_colormap.m` script above, change file name from 
`file_name='output_4.000000_0.000100_10.000000.mat';`
to
`file_name='initial_concentration.mat';`
and then execute the script. To execute the .m script using Octave, see below. 

In order to load Octave, we needed to load the following modules (on Marenostrum MN4):
```
1) gcc/7.2.0   2) impi/2018.2   3) mkl/2018.2   4) suitesparse/5.2.0   5) octave/4.2.2
```
Save the script above and run. 

If everything goes well the "2-D colormap" or the "3-D surface" should appear !

## Running the tumor growth example : 3D_tumor_example_X.cpp in ./examples/

Steps:
1. From the root directory, execute $ make 3DtumorX. This should produce an executable named 3DtumorX in `./examples/ directory`.
1. A help script named `script_3DtumorX.sh` is given in the root directory and to submit to SLURM, execute `$ sbatch script_3DtumorX.sh`. As in the previous example, 2 MPI processes with 24 threads each are spawned to execute 3DtumorX. 
1. After the execution, there will be multiple .mat files created in the root directory. These .mat files can be plotted using the plotting_microenv_tumor.py Python script in the `./matlab/` directory to have a 2D plot of the 3Dal microenvironment.
1. The `plotting_microenv_tumor.py` script needs two arguments: the name of the mat file and the value of the cut in the z axis that is to be plotted. Note: The z value chosen need to be a value in the range of the example simulated, so you need to be knowledgeable of the range of values in the z dimension in the mat file. For instance, for the above example, z will have a range of [0.025, 3.975] with increments of 0.05.
    1. The script can be run with: `python plotting_microenv.py "filename.mat" z_cut`. 
    1. The PNG generated is named using both arguments "filename_z_cut.png"

## How to make new examples

If readers have followed the tutorial until here, they will have compiled, run and analysed our examples. Nevertheless, writing new examples should be the goal of future users of BioFVM-X, but these efforts need understanding of C++, Mathematics, Physics and Biology.

To ease these efforts, we have tried to write our code in a way that is easy to incorporate its functions into other examples. We know that there might be users that still want step-by-step tutorials on how to build their new examples. These users can find these in the blog from the BioFVM developers:

- [BioFVM warmup: 2D continuum simulation of tumor growth](http://www.mathcancer.org/blog/biofvm-warmup-2d-continuum-simulation-of-tumor-growth/)
- [Building a 3D Cellular Automaton Model Using BioFVM](http://www.mathcancer.org/blog/building-a-cellular-automaton-model-using-biofvm/)


# Original BioFVM README file
```
####################################################
BioFVM: Finite Volume Solver for Biological Problems.
####################################################

Version:      1.1.6
Release date: 19 August 2017

Homepage:     http://BioFVM.MathCancer.org
Downloads:    http://BioFVM.sf.net

Summary:  
This update includes small bug fixes and enhancements for 
compatibiltiy with PhysiCell. It is focused on improving 
its handling of default settings and Dirichlet conditions. 

New features:
+ Added ability to enable / disable Dirichlet conditions for 
  individual substrates. Use: 
  
  void Microenvironment::set_substrate_dirichlet_activation(
       int substrate_index , bool new_value )
  
  to set substrate with index substrate_index to apply or 
  not apply Dirichlet conditions, for all defined Dirichlet 
  nodes. (e.g., you want to set Dirichlet conditions for the
  first substrate, but not teh others). 
  
+ Added Boolean new use_oxygen_as_first_field to Microenvironment_Options,
  so that initialize_microenvironment() only uses these if the flag 
  is set to true. By default, it is set to true. 
  
+ Calling "Microenvironment.set_density()" or 
  "Microenvironment.add_density()" now sets: 
  
  default_microenvironment_options.use_oxygen_as_first_field = false
  
  so that calling initialize_microenvironment() after defining fields 
  does not overwrite prior user choices. 
  
  These functions also correctly resize 
  
  default_microenvironment_options.Dirichlet_condition_vector and 
  default_microenvironment_options.Dirichlet_activation_vector
  
+ Updated all the add/update_density functions in the Mircoenvironment,
  so that adding a new density sets   
  
+ Updated Microenvironment::apply_dirichlet_conditions() so that 
  it only applies to the substrates j for which 
  
  microenvironment.dirichlet_activation_vector[j] == true 

Bugfixes: 
+ Fixed initialize_microenvironment() so that it now only applies Dirichlet 
  conditions if 
  
  default_microenvironment_options.outer_Dirichlet_conditions == true 
  
+ Updated the makefiles to use ARCH := native, which simplifies setup. 
  This generally has your compiler query your processor for available 
  hardware optimizations, so you don't have to choose this yourself 
  (e.g., ARCH := haswell). I suggest using a compiler at least as 
  recent as gcc/g++ 6.5.x   
  
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
  
Version:      1.1.5
Release date: 19 July 2017

Homepage:     http://BioFVM.MathCancer.org
Downloads:    http://BioFVM.sf.net

Summary:  
This update includes small bug fixes and enhancements for 
compatibiltiy with PhysiCell. 

New features:
+ Added Microenvironment::find_substrate_index( std::string ) to make it 
  easier to find . 
  
+ Added Basic_Agent::nearest_gradient( int substrate_index ) to directly 
  access the gradient of the indicated substrate at the agent's  
  current_voxel_index. 
  
+ Added Basic_Agent::nearest_gradient_vector( void ) to directly 
  access a vector of gradients (one for each substrate in the 
  microenvironment) at the agent's current_voxel_index. 
 
+ Added Microenvironment::is_dirichlet_node( int voxel_index ) to 
  easily check if the voxel is a Dirichlet node. 
  
+ Added new class Microenvironment_Options, with a default 
  default_microenvironment_options, to simplify Microenvironment
  setup. The defaults are dx = dy = dz = 20 microns, on 1 cubic mm. 
  
+ Added function initialize_microenvironment() to set up the 
  microenvironment using the options in 
  default_microenvironment_options. The code sets oxygen as the 
  default field, with D = 1e5 micron^2/min, and decay rate = 0.1 1/min
  (a 1 mm diffusion length scale). If 
  default_microenvironment_options.outer_Dirichlet_conditions = true, 
  then we set a 38 mmHg condition on the outer edges, corresponding to
  5% oxygenation (physioxic conditions). 

+ Updated the makefile to default to MARCH := native, which allows
  automated performance tuning without user editing of the Makefile.

Bugfixes: 
+ correct typos in citation information in all source files 

+ updated citation information 

+ added void set_default_microenvironment( Microenvironment* M ) declaration to 
  BioFVM_Microenvironment.h
 
+ set volume_is_changed = false in Basic_Agent::set_internal_uptake_constants(); 

+ Set the MultiCellDS options Booleans to extern bool in BioFVM_MultiCellDS.h 
  so that PhysiCell can read these options. 

+ Updated the simlified_data field in MultiCellDS XML output to include a 
  new "source" attribute with value "BioFVM".

+ Updated Microenvironment::update_dirichlet_node() and 
  Microenvironment::remove_dirichlet_node() to check against 
  mesh.voxels[].is_Dirichlet to provide a cheap and early exit 
  if the node isn't Dirichlet in the first place. 
  
+ Changed to a thread-safe data structure for Dirichlet nodes 
  (e.g., if a custom cell function in an OMP loop adds or removes 
  Dirichlet nodes). 
  
+ Fixed convergence_test3.cpp, convergence_test4_1.cpp, 
  convergence_test4_2.cpp, convergence_test5.cpp, 
  performance_test_numcells.cpp, tutorial1_BioFVM.cpp, 
  and main_experiment.cpp to use Basic_Agent::set_total_volume(). 

+ Fixed tutorial3_BioFVM.cpp to use dirichlet_one instead of 
  dirichlet_zero. 
```
